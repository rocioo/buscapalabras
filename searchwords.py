#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''
import sys

import sortwords

def search_word(word, words_list):
    lista_minus   = [palabra.lower() for palabra in words_list]
    palabra_minus = word.lower()

    if palabra_minus in lista_minus:
        test_index=lista_minus.index(palabra_minus)
        return test_index
    if not palabra_minus in lista_minus:
        raise Exception




def main():
    palabra=sys.argv[1]
    lista=sys.argv[2:]
    ordered_list=sortwords.sort(lista)
    sortwords.show(ordered_list)
    try:
        position=search_word(palabra,ordered_list)
        print(position)
    except:
        Exception
        sys.exit()


if __name__ == '__main__':
    main()
